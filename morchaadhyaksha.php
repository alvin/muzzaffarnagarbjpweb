<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>मोर्चा अध्यक्ष - भारतीय जनता पार्टी</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

	<!-- CSS Files -->
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/aos/aos.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
	<link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

	<!-- Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

	<!-- ======= Header ======= -->
	<?php include('header.php') ?>
	<!-- end heder -->

	<main id="main" style="margin-top: 10px;">

		<section id="pricing" class="pricing">
			<div class="container-fluid">
				<div class="text-center mb-4" style="background-color: #f3f1f0; height: 40px;">
					<strong>
						<h2>मोर्चा अध्यक्ष</h2>
					</strong>
				</div>

				<div class="row">
					<div class="col-lg-3 mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/prashudatt-diwedi.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्री प्रान्शुदत्त द्विवेदी</h4>
							</span>
							<span>
								<h5> युवा मोर्चा - अध्‍यक्ष</h5>
							</span>
							<hr>
							<span>
								<p>2/22, साहबगंज, नारायणदास, फर्रूखाबाद</p>
								<p>pranshuduttdwivedi@gmail.com</p>
							</span>
							<div class="btn-wrap">
								<a href="https://www.facebook.com/PranshuDuttDwivedi" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
								<a href="https://twitter.com/PranshuDutt" class="btn-icon"><i class="fab fa-twitter-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3  mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/geeta-shakya.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्रीमती गीता शाक्‍य</h4>
							</span>
							<span>
								<h5>महिला मोर्चा - अध्‍यक्ष</h5>
							</span>
							<hr>
							<span>
								<p>जवाहर नगर, विधूना जनपद, औरैया</p>
								<p>geetashakyabidhuna@gmail.com</p>
							</span>
							<div class="btn-wrap">
								<a href="https://www.facebook.com/geetashakyaUP" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
								<a href="https://twitter.com/geetashakyaUP" class="btn-icon"><i class="fab fa-twitter-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3  mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/kameshwar-singh.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्री कामेश्वर सिंह</h4>
							</span>
							<span>
								<h5>किसान मोर्चा - अध्‍यक्ष</h5>
							</span>
							<hr>
							<span>
								<p>C/o होटल आवन्तिका, मोहद्दीपुर, गोरखपुर</p>
								<p>crv3108@gmail.com</p>
							</span>
							<div class="btn-wrap">
								<a href="" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
								<a href="" class="btn-icon"><i class="fab fa-twitter-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3  mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/Narendra-Kashyap.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्री नरेन्द्र कश्यप (पूर्व सांसद)</h4>
							</span>
							<span>
								<h5>ओ0बी0सी0 मोर्चा - अध्‍यक्ष</h5>
							</span>
							<hr>
							<span>
								<p>म0नं0-61, ब्लाक-बी, संजय नगर, सेक्टर-23</p>
								<p>गाजियाबाद</p>
							</span>
							<div class="btn-wrap">
								<a href="" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
								<a href="" class="btn-icon"><i class="fab fa-twitter-square mx-2"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-3 mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/blank-profile-picture-6.png" style="max-width: 154px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्री रामचन्द्र कनौजिया
								</h4>
							</span>
							<span>
								<h5>अनुसूचित जाति मोर्चा - अध्‍यक्ष
								</h5>
							</span>
							<hr>
							<span>
								<p>
									बीबीगंज, सआदतगंज, लखनऊ
								</p>
							</span>
							<div class="btn-wrap">
								<a href="https://www.facebook.com/RamChandra-Kannoujiya-342751382818741" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3  mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/Sanjay-Gaud.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>श्री संजय गोण्ड़

								</h4>
							</span>
							<span>
								<h5>अनुसूचित जनजाति मोर्चा - अध्‍यक्ष
								</h5>
							</span>
							<hr>
							<span>
								<p>
									ग्राम व पोस्ट हथौजा, बलिया
								</p>
							</span>
							<div class="btn-wrap">
								<a href="" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3  mb-4" data-aos="fade-up">
						<div class="box">
							<img src="assets/img/morchaimage/basit-ali.webp" style="max-width: 180px;" class="img-fluid mb-2" alt="">
							<span>
								<h4>कु0 बासित अली
								</h4>
							</span>
							<span>
								<h5> अल्पसंख्यक मोर्चा - अध्‍यक्ष

								</h5>
							</span>
							<hr>
							<span>
								<p>
									म0नं0-406, सेक्टर-11, शास्त्री नगर, मेरठ
								</p>
							</span>
							<div class="btn-wrap">
								<a href="" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
							</div>
						</div>
					</div>

					<div class="col-lg-3"></div>
				</div>
			</div>
			</div>
		</section>
	</main><!-- End #main -->

	<!-- ======= Footer ======= -->
	<?php include('footer.php') ?>

	<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/aos/aos.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
	<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>

</body>

</html>
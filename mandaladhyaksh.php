<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>मंडल अध्यक्ष - भारतीय जनता पार्टी, बिजनौर</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

    <!-- ======= Header ======= -->
    <?php include('header.php') ?>
    <!-- end heder -->


    <main id="main">

        <section id="pricing" class="pricing">
            <div class="container-fluid">
                <div class="text-center mb-5" style="background-color: #f3f1f0; height: 40px;">
                    <strong>
                        <h2>मंडल अध्यक्ष</h2>
                    </strong>
                </div>

                <div class="row">
                    <div class="text-center mb-2" style="height: 40px;">
                        <strong>
                            <h4>नजीबाबाद</h4>
                        </strong>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री मुकुल रंजन दीक्षित</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>नजीबाबाद</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9012627755</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री राजकुमार प्रजापति
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>भागुवाला</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    8535056650
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री वीरेंद्र शर्मा</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>नांगल सोती</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9457205006</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री जुग्नेश कुमार
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>गजरौला पाईमार</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    8535056650
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center mb-2" style="height: 40px;">
                        <strong>
                            <h4>नगीना</h4>
                        </strong>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री नीरज विश्नोई</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>नगीना</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9760635999</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री दिनेश त्यागी
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>महेश्वरी</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9837312911
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री योगेंद्र राजपूत</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>कीरतपुर नगर</h5>
                            </span>
                            <hr>
                            <span>
                                <p>7409822822</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री मनोज सिंह
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>कीरतपुर ग्रामीण</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    8535056650
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center mb-2" style="height: 40px;">
                        <strong>
                            <h4>वढापुर</h4>
                        </strong>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>विजयपाल सिंह</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>वढापुर</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9758638746</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री मुकेश शर्मा
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>अफजलगढ़</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9411429574
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री देशराज सिंह</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>कासमपुर गढ़ी</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9837192064</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री संदीप वर्मा
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>रायपुर सादात</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    8899686839
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center mb-2" style="height: 40px;">
                        <strong>
                            <h4>नहटौर </h4>
                        </strong>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री सिद्धार्थ जैन </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>नहटौर</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9927585490</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री दर्पण रावल
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>हल्दौर </h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9927025414
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री दानवीर सिंह </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>भटियाना </h5>
                            </span>
                            <hr>
                            <span>
                                <p>9719312564</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री राजेश त्यागी
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>आकु</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9837411406
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center mb-2" style="height: 40px;">
                        <strong>
                            <h4>बिजनौर</h4>
                        </strong>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री संजीव गुप्ता</h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>बिजनौर नगर</h5>
                            </span>
                            <hr>
                            <span>
                                <p>9720165915</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री राजीव राजपूत
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>मंडावर</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9457067466
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>श्री दानवीर सिंह </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>भटियाना </h5>
                            </span>
                            <hr>
                            <span>
                                <p>9719312564</p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 mb-4">
                        <div class="box" data-aos="fade-up">
                            <span>
                                <h4>
                                    श्री राजेश त्यागी
                                </h4>
                            </span>
                            <span>
                                <h5>मंडल अध्यक्ष</h5>
                            </span>
                            <hr>
                            <span>
                                <h5>आकु</h5>
                            </span>
                            <hr>
                            <span>
                                <p>
                                    9837411406
                                </p>
                            </span>
                            <div class="btn-wrap">
                                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </section>
    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php include('footer.php') ?>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>
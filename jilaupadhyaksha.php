<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>जिला उपाध्यक्ष - भारतीय जनता पार्टी, बिजनौर</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

  <!-- CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <?php include('header.php') ?>
  <!-- end heder -->


  <main id="main" style="margin-top: 10px;">

    <section id="pricing" class="pricing">
      <div class="container-fluid">
        <div class="text-center mb-4" style="background-color: #f3f1f0; height: 40px;">
          <strong>
            <h2>जिला उपाध्यक्ष</h2>
          </strong>
        </div>

        <div class="row">
          <div class="col-lg-3 mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/Dineshsanijilaupkdhyas.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्री दिनेश सैनी</h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>9759699671</p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/Vedprakeshjijilaupadhyas.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>
                  श्री वेदप्रकाश विश्वकर्मा
                </h4>
              </span>
              <span>
                <h5> जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  9411832820
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3  mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/pramodchauhanjilaupadhays.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्री प्रमोद कुमार चौहान</h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  8445676029
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3  mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/Subhaschauhanjilaupadhyas.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्री सुभाष चौहान
                </h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  9412117361
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://twitter.com/kanta_kardam" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-3 mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/poonamgoyaljilaupadhayaksha.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्रीमती पूनम गोयल
                </h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  8868815478
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3  mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/sangitaaggarwaljilaupadhyaksha.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्रीमती संगीता अग्रवाल
                </h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  9456903701
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/arvindgahlautjilaadhyaskha.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्री अरविंद गहलौत
                </h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  9456409813
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 mb-4">
            <div class="box" data-aos="fade-up">
              <img src="assets/img/jilaupadhyaksha/pushkarsingh.jpg" style="max-width: 180px;" class="img-fluid mb-2" alt="">
              <span>
                <h4>श्री पुष्कर सिंह
                </h4>
              </span>
              <span>
                <h5>जिला उपाध्यक्ष </h5>
              </span>
              <hr>
              <span>
                <p>
                  9917280151
                </p>
              </span>
              <div class="btn-wrap">
                <a href="https://www.facebook.com/satyapalsinghsainisansad" class="btn-icon"><i class="fab fa-facebook-square mx-2"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php include('footer.php') ?>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
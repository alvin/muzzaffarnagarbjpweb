<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>संपर्क - भारतीय जनता पार्टी</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

    <?php include('header.php') ?>

    <main id="main">
        <!-- ======= Section ======= -->
        <section id="pricing" class="pricing">
            <div class="container-fluid">
                <div class="text-center mb-4" data-aos="fade-down" style="background-color: #f3f1f0; height: 40px;">
                    <strong>
                        <h2>संपर्क</h2>
                    </strong>
                </div>

                <div class="row justify-content-center">
                <div class="col-lg-4 mb-4">
                        <div class="box" data-aos="fade-up">
                            <img src="assets/img/karyalaysampark/Vinayrana.jpg" class="img-fluid" alt="">
                            <span>
                                <h4>श्री विनय राणा</h4>
                            </span>
                            <span>
                                <h5>कार्यालय सह-प्रभारी</h5>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4 mb-4">
                        <div class="box" data-aos="fade-up">
                            <img src="assets/img/karyalaysampark/gaurav.jpg" class="img-fluid" alt="">
                            <span>
                                <h4>श्री गौरव कुमार</h4>
                            </span>
                            <span>
                                <h5>कार्यालय संपर्क (कंप्यूटर ऑपरेटर)</h5>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container my-4">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="box" data-aos="fade-down">
                            <span class="mb-4">
                                <h3 style="color: #fc7304; font-size: 25px; font-weight: 600;">कार्यालय पता :</h3>
                            </span>
                            <span>
                                <h4>
                                    <p>सरोजनी नगर, नजीबाबाद रोड, बिजनौर</p>
                                    <p>
                                        +91 8938953372, 8287243859
                                    </p>
                                    <p>
                                        office.bjpbijnor@gmail.com
                                    </p>
                                </h4>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Section -->
    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php include('footer.php') ?>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>
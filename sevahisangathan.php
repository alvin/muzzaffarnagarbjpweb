<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>सेवा ही संगठन : E-बुक, भारतीय जनता पार्टी, बिजनौर</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!-- <link href="assets/img/" rel="icon"> -->
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="assets/css/all.css"> -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
    <?php include('header.php') ?>

    <!-- ======= Carousel  ======= -->
    <section>
        <div class="text-center mb-4" data-aos="fade-up">
            <h2 style="margin-top: 10px; font-weight: 600;">
                सेवा ही संगठन : E-बुक
            </h2>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                                class="active" aria-current="true"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="1"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="2"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="3"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="4"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="5"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="6"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="7"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="8"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="9"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="10"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="11"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="12"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="13"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="14"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="15"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="16"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="17"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="18"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="19"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="20"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="21"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="22"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="23"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="24"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="25"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="26"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-001-1-768x1109.webp"
                                    class="img-fluid d-block w-100" alt="img-1">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-002-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-2">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-003-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-3">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-004-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-4">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-006-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-5">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-007-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-6">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-008-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-7">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-009-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-8">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-010-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-9">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-011-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-10">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-012-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-11">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-013-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-12">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-018-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-13">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-016-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-14">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-012-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-15">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-018-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-16">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-016-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-17">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-021-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-18">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-023-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-19">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-024-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-20">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-025-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-21">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-026-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-22">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-031-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-23">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-028-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-24">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/seva_hi_sangathan_img/SEWA-HI-SANGTHAN_UP_HARSH_compressed-page-031-768x1109.webp"
                                    class="d-block img-fluid w-100" alt="img-25">
                            </div>
                            <!-- <div class="carousel-item">
                                <img src="..." class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="..." class="d-block w-100" alt="...">
                            </div> -->
                        </div>
                        <button class="carousel-control-prev" style="background-color: rgb(243, 243, 243);"
                            type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <i class="fas fa-angle-left" style="font-size: 50px; color: rgb(162, 165, 3);"></i>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" style="background-color: rgb(243, 243, 243);"
                            type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <i class="fas fa-angle-right" style="font-size: 50px;"></i>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- carousel end -->

    <?php include('footer.php') ?>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

    <!-- Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>